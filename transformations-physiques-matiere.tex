%transformations-physiques-matiere
\chapter[Les transformations physiques]{Les transformations physiques de la matière}
\label{chap:transformations-physiques}

L'un des premiers éléments pour décrire un objet est son état physique. 
Comme il existe plusieurs états physiques, des notions ont été inventées 
pour nommer le passage d'un état physique à un autre.

\section{Rappel~: Les états de la matière}
\label{sec:etats-matiere-rappel}
\aretenir{La matière se présente sous trois états physiques~:
\begin{itemize}
	\item l'état solide où les atomes sont serrés, ordonnés et liés les 
	uns aux autres,
	\item l'état liquide où les atomes sont serrés et désordonnés les 
	uns des autres,
	\item l'état gazeux où les atomes sont dispersés et très désordonnés 
	les uns des autres.
\end{itemize}
}%fin du aretenir

\section{Diagramme des transformations physiques de la matière}
\label{sec:transfo-physique-matiere}
\aretenir{Lorsqu'un corps change uniquement d'état physique, il subit une 
transformation physique. Existent six transformations physiques dont les 
noms et les sens sont déductibles d'après le diagramme suivant.
}%fin du aretenir
\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=0.85]
		\foreach \abscisse/\legende in {0/S o l i d e, 5/L i q u i d e, %
		10/G a z ou V a p e u r}{%
			\draw[rounded corners, fill=white!80!black, fill opacity=0.333] (\abscisse,0) rectangle ++(4,1.5) ;
			\node at (\abscisse+2,0.75) {\legende} ;
		} ;
		% Flèches du haut
		\draw[->] (3,1.5) -- ++(0.5,0.5) ;
		\draw[->] (5.25,2.25) -- ++(0.5,-0.5) ;
		\draw[->] (8,1.5) -- ++(0.5,0.5) ;
		\draw[->] (10.25,2.25) -- ++(0.5,-0.5) ;
		\draw (3,1.5) -- ++(0.75,0.75) -- ++(1.5,0) -- ++(0.75,-0.75) ;
		\draw (8,1.5) -- ++(0.75,0.75) -- ++(1.5,0) -- ++(0.75,-0.75) ;
		% Flèches du bas
		\draw[->] (3.75,-0.75) -- ++(-0.5,0.5) ;
		\draw[->] (6,0) -- ++(-0.5,-0.5) ;
		\draw (3,0) -- ++(0.75,-0.75) -- ++(1.5,0) -- ++(0.75,+0.75) ;
		\draw[->] (8.75,-0.75) -- ++(-0.5,0.5) ;
		\draw[->] (11,0) -- ++(-0.5,-0.5) ;
		\draw (8,0) -- ++(0.75,-0.75) -- ++(1.5,0) -- ++(0.75,+0.75) ;
		% Grandes flèches
		\draw (1,1.5) -- ++(1.5,1.75) -- ++(9,0) -- ++(1.5,-1.75) ;
		\draw (1,0) -- ++(1.5,-1.75) -- ++(9,0) -- ++(1.5,1.75) ;
		\draw[->] (1,1.5) -- ++(0.75,0.875) ;
		\draw[->] (11.5,3.25) -- ++(0.75,-0.875) ;
		\draw[->] (13,0) -- ++(-0.75,-0.875) ;
		% Légendes
		\node at (7,3.5) {S u b l i m a t i o n} ;
		\node at (4.5,2.5) {Fusion} ;
		\node at (9.5,2.5) {Vaporisation} ;
		\node at (4.5,-1) {Solidification} ;
		\node at (9.5,-1) {Liquéfaction} ;
		\node at (7,-2) {C o n d e n s a t i o n} ;
	\end{tikzpicture}
	\caption{Le diagramme des transformations physiques.}
\end{figure}
Exprimé autrement on a~:
\begin{itemize}
	\item de l'état solide à l'état liquide, la fusion,
	\item de l'état liquide à l'état gazeux, la vaporisation,
	\item de l'état gazeux à l'état liquide, la liquéfaction,
	\item de l'état liquide à l'état solide, la solidification,
	\item de l'état solide à l'état gazeux (directement), la 
	sublimation,
	\item de l'état gazeux à l'état liquide (directement), la 
	condensation.
\end{itemize}

\section{La masse pendant une transformation physique}
\label{sec:masse-transfo-physique}
Voici une simple expérience réalisable y compris à la maison. On place un 
récipient avec une quantité d'eau (et un bouchon non dessiné) au 
congélateur, puis quelques temps plus tard, une à deux bonnes heures, on 
sort le récipient et on le pèse. 

Puis on laisse la glace fondre tout en laissant bouché le récipient. 
Pensez aussi à tenir compte de l'humidité qui va venir s'ajouter à la 
surface extérieure du verre dont il faudra tenir compte.
\begin{figure}[H]
	\centering
		\begin{tikzpicture}[scale=0.7071]
			\fill[yellow!25!white] (8,0) rectangle ++(2.5,1.5) ;
			\fill[yellow!25!white] (0,0) rectangle ++(2.5,1.75) ;
			\foreach \abscisse/\etape in {0/début,8/fin}{%
				\node at (\abscisse+1,3.5) {\etape} ;
				\draw[rounded corners, line width=3, color=blue!40!white] (\abscisse, 3) -- ++(0,-3) -- ++(2.5,0) -- ++(0,2.75) -- ++(0.25,0.25) ;
				\draw[line width=2] (\abscisse-0.25,-0.1) -- ++(3,0) ;
				\draw[line width=2] (\abscisse+1.25,-0.1) -- ++(0,-0.1) ;
				\draw[rounded corners, fill=white!80!black] (\abscisse-0.25,-0.2) rectangle ++(3,-0.9) ; 
				\fill[white] (\abscisse,-0.3) rectangle ++(1.5,-0.5) ; 
			} ;
			% Flèches des légendes
			\draw[<-, purple] (2.55,2.5) -- ++(2,0) node[right] {Bécher} ;
			\draw[<-, purple] (1.5,1) -- ++(3.05,0) node[right] {Eau solide} ;
			\draw[<-, purple] (9.5,1) -- ++(3.05,0) node[right] {Eau liquide} ;
		\end{tikzpicture}
	\caption{Expérience d'observation de la masse lors d'une transformation physique}
\end{figure}
Au final, vous observerez que la masse est la même au début et à la fin 
en tenant compte de l'imprécision des balances électroniques et de leurs 
dérives.

Si on fait le bilan de l'expérience~:
\begin{itemize}
	\item le récipient, le bouchon et l'air n'ont pas changé, leurs 
	masses sont donc les même,
	\item la glace a fondu, l'eau a subit une transformation physique, 
	qui est la fusion,
	\item la masse globale n'ayant pas changée, cela signifie que la 
	masse de l'eau n'a pas changé non plus malgré la fusion.
\end{itemize}

\aretenir{Lors d'une transformation physique la masse reste constante.
}%fin du aretenir

\section{Le volume pendant une transformation physique}
\label{sec:volume-transfo-physique}

\begin{figure}[H]
	\centering
		\begin{tikzpicture}[scale=0.7071]
			\fill[yellow!25!white] (0,0) rectangle ++(2.5,1.5) ;
			\fill[yellow!25!white] (8,0) rectangle ++(2.5,1.75) ;
			\draw[dashed] (0,1.5) -- ++(2.5,0) ;
			\draw[dashed] (8,1.5) -- ++(2.5,0) ;
			\foreach \abscisse/\etape in {0/début,8/fin}{%
				\node at (\abscisse+1,3.5) {\etape} ;
				\draw[rounded corners, line width=3, color=blue!40!white] (\abscisse, 3) -- ++(0,-3) -- ++(2.5,0) -- ++(0,2.75) -- ++(0.25,0.25) ;
			} ;
			% Flèches des légendes
			\draw[<-, purple] (2.55,2.5) -- ++(2,0) node[right] {Bécher} ;
			\draw[<-, purple] (1.5,1) -- ++(3.05,0) node[right] {Eau liquide} ;
			\draw[<-, purple] (9.5,1) -- ++(3.05,0) node[right] {Eau solide} ;
		\end{tikzpicture}
	\caption{Expérience d'observation du volume lors d'une transformation physique}
\end{figure}

......

\begin{center}
	\pgfornament[width=0.33\linewidth, color=teal]{84}
\end{center}
